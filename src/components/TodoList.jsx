import PropTypes from "prop-types";
import TodoItem from "./TodoItem";

function TodoList(props) {
  const { todos } = props;
  // const list = todos.map((item) => <li>{item.title} {item.completed}</li>);
  /*husizhdueizhdeuizdheuizhdezgdzigde
  dkoepzdoepzjde
  edjizodheuzi*/
  return (
    <div>
      <h1>My todo list ({todos.length} items):</h1>
      <ul>
        {todos.map((item) => ( // todos est un tableau, et utilise la propriété map pour parcourir le tableau et le recréer
          <TodoItem key={item.id} {...item} /> // Pour afficher les éléments du tableau, on appelle le composant TodoItem, et on le décompose(...item).
          //Pour résoudre l'erreur "Each child in a list should have a unique “key” prop."
          //React a besoin d'un id pour afficher les éléments selon certaines conditions (ordre, fini, pas commencé...)
        ))}
      </ul>
    </div>
  );
}

TodoList.propTypes = {
  todos: PropTypes.array.isRequired,
};

export default TodoList;
